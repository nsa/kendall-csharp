# Kendall's W

## Setup (if current machine is not Windows and/or there no Visual/Xamarin Studio):
[Install ASP.NET5](https://docs.asp.net/en/latest/getting-started/index.html)

### Windows:
		$ dnu upgrade

### OSX:
		$ curl -sSL https://raw.githubusercontent.com/aspnet/Home/dev/dnvminstall.sh | DNX_BRANCH=dev sh && source ~/.dnx/dnvm/dnvm.sh
		$ dnvm upgrade -r mono		
		
### Both:		
		$ dnu restore
		
## Build/Run From Commandline				
		$ dnu build
		$ dnx run
