﻿using System.Collections.Generic;

namespace Kendall
{
    class CalcKendallCl
    {   
        /// <summary>
        /// вычиление коэффициента Кендалла 
        /// </summary>
        /// <param name="Array"></param>
        /// <returns></returns>
        public IDictionary<string, dynamic> CalcKendall(float[,] Array)
        {
            IDictionary<string, dynamic> dictionaryResult = new Dictionary<string, dynamic>();
            int m = Array.GetLength(0);  //количество вопросов (строки)
            int n = Array.GetLength(1);  //количество экспертов (столбцы)

            if (m >= 1 && n >= 1)
            {
                matchedData arrayData = new matchedData(Array);
                friedmanTest resullTest = new friedmanTest(arrayData);
                dictionaryResult.Add("W", resullTest.W);
                dictionaryResult.Add("C", resullTest.C);
                dictionaryResult.Add("M", resullTest.M);
                dictionaryResult.Add("S", resullTest.S);   
                dictionaryResult.Add("ranks", resullTest.getRanks());                              
            }
  
            return dictionaryResult;
        }
    }
}
