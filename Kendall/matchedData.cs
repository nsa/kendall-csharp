﻿using System;
using System.Text;

namespace Kendall
{
    public class matchedData
    {
        private double[,] array;
        private int n;
        private int countExperts;
        private int countFactors;

        public matchedData(float[,] Array)
        {       
            this.countFactors = Array.GetLength(0);
            this.countExperts = Array.GetLength(1);
            

            this.array = new double[countFactors,countExperts];
            for (int i = 0; i < this.countFactors; i++)
                for (int j = 0; j < this.countExperts; j++)
                {
                    this.array[i, j] = Array[i, j];
                }

                    this.n = this.countFactors * this.countExperts;
        }

        public matchedData(matchedData arrayData)
        {
            this.countExperts = arrayData.countExperts;
            this.countFactors = arrayData.countFactors;

            this.array = arrayData.array;
            this.n = this.countExperts * this.countFactors;
        }

        public string[] getMass(int n)
        {
            string[] returnMass = new String[n];
            for (int i = 0; i < n; i++)
            {
                returnMass[i] = i.ToString();
            }
            return returnMass;
        }

        public int getCountExperts () 
        {
            return this.countExperts;
        }

        public int getCountFactors()
        {
            return this.countFactors;
        }
        
        public string toString() {
            StringBuilder returnString = new StringBuilder();
            returnString.Append("\nMatched data\n");
            
            for (int j = 0; j < this.countFactors; j++) {
                for (int i = 0; i < this.countExperts; i++) returnString.Append("\t" + this.array[j,i]);
                returnString.Append("\n");
            }
            return returnString.ToString();
        }

        internal int getRankByExpert()
        {
            int k = 0;
            for (int i = 0; i < this.countFactors; i++)
            {
                rank localrank = new rank( getData(i));
                k += localrank.GetCorrectionFactor();
                for (int j = 0; j < this.countExperts; j++)
                {
                    this.array[i,j] = localrank.getDataRank(j);
                }
            }
                return k;
        }

        private double[]  getData(int i)
        {
            double[] returnMass = new double[this.array.GetLength(1)];
            for(int s=0; s< returnMass.GetLength(0); s++)
            {
                returnMass[s] = array[i,s];
            }
            return returnMass;  
        }

        internal double[,] getArray()
        {
            return this.array;
        }
    }
}



