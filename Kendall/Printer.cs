using System;
using System.Collections.Generic;

namespace Kendall
{
    public class Printer
    {
        public void Print ()
        {          
            float[,] RankArray, RankTArray;                      
            IDictionary<string, dynamic> Result, ResultT;
            CalcKendallCl pr;
            
            pr = new CalcKendallCl();            
            RankArray = Data.RightDirection();
            Result = pr.CalcKendall(RankArray);
            PrintResult(Result);
            
            RankTArray = Data.TransposedDirection();
            ResultT = pr.CalcKendall(RankTArray);
            Console.WriteLine("\nTRANSPOSED:");
            PrintResult(ResultT);
            
            Console.Read();
        }
        
        private void PrintResult (IDictionary<string, dynamic> result)
        {
            Console.WriteLine(result["ranks"]);
            Console.WriteLine("Sum of squares of rank deviations, S: " + result["S"]);
            Console.WriteLine("Correction factor for ties, C: " + result["C"]);
            Console.WriteLine("Friedman test statistic, M: " + result["M"]);
            Console.WriteLine("Kendall's W: " + result["W"]);
        }
    }    
}
