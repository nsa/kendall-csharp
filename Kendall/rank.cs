﻿using System;

namespace Kendall
{
    public class rank
    {
        private int[] r;
        private double[] dataRank;
        private int n;
        private int s;
        private int t;

        public rank(double[] p)
        {
            int length = p.GetLength(0);
            this.n = length;

            double[] array1 = new double[length];
            double[] array2 = new double[length];

            this.r = new int[length];

            for (int i = 0; i < length; i++)
            {
                this.r[i] = i;
                array2[i] = p[i];
            }

            this.s=0;
            this.t=0;

            sort(array2, this.r, --length);
            array1[length] = length;

            for (int j = 0; j < length; j++)
            {
                if (Math.Abs(array2[j] - array2[j + 1]) > 0.0)
                {
                    array1[j] = j;
                }
                else
                {
                    int k = 1;
                    for (int i = j + 1; i < length; i++, k++)
                    {
                        if (Math.Abs(array2[i] - array2[i + 1]) > 0.0)
                            break;
                    }

                    double d = j + k * 0.5;
                    for (int ii = 0; ii <= k; ii++)
                        array1[j + ii] = d;

                    int m = k * (k + 1);
                    this.s += m;
                    this.t += m * (k + 2);
                    j += k;
                }
            }
            length++;
            this.dataRank = new double[length];

            for (int i = 0; i < length; i++)
                this.dataRank[this.r[i]] = array1[i] + 1;

        }

        private void sort(double[] array, int[] p, int lenght)
        {
           for ( int j=0; j< lenght; j++ )
               for (int i = 0; i < lenght-j; i++)
               {
                   if (array[i] > array[i + 1])
                   {
                       double b = array[i];
                       array[i] = array[i + 1];
                       array[i + 1] = b;
                       b = p[i];
                       p[i] = p[i + 1];
                       p[i + 1] = Convert.ToInt32( b);
                   }
               }
        }


        internal int GetCorrectionFactor()
        {
            return this.t;
        }

        internal double getDataRank(int j)
        {
            return this.dataRank[j];
        }
    }
}
