﻿namespace Kendall
{
    public class friedmanTest
    {
        private matchedData ranksData;
        private int n;
        private int k;
        public double C { get; }
        public double M { get; }
        public double S { get; }
        public double W { get; }

        public friedmanTest(matchedData arrayData)
        {
            int m;
            double d1;
            this.S = 0.0;
            this.k = arrayData.getCountExperts();   //эксперты
            this.n = arrayData.getCountFactors(); //вопросы

            if (this.n  >= 2 && this.k >= 2)
            {
                this.ranksData = new matchedData(arrayData);
                double d2 = (this.k + 1) * 0.5;
                m = this.ranksData.getRankByExpert();
                double[,] array = this.ranksData.getArray();

                for (int j = 0; j < this.k; j++)
                {
                    d1 = 0;
                    for (int i = 0; i < this.n; i++)
                    {
                        d1 += array[i, j];
                    } 
                    d1 /= this.n;
                    this.S += (d1 - d2) * (d1 - d2);
                }
                double d3 = this.k * (this.k + 1);
                double d4 = this.k * (this.k * this.k - 1);

                this.C = (1.0D - m / (this.n * d4));
                this.M = (12.0D * this.n * this.S / d3 / this.C);
                this.W = (this.M / (this.n * (this.k - 1.0D)));
            }
            else
            {
                this.M = 0.0;
                this.W = 0.0;
                this.C = 0.0;
            }
        }
        
        public string getRanks()
        {
            return this.ranksData.toString();    
        }
    }
}
